
############################################################################
############################################################################
###                                                                      ###
###             SHINY APPLICATION FOR DOE UTILISATION PRIZE              ###
###                       UPDATED AS OF 2023-07-17                       ###
###                                                                      ###
############################################################################
############################################################################

# Rewrite Banner Before Publication
bannerCommenter::copy_to_clipboard(
  bannerCommenter::banner(paste0("Shiny Application for DoE Utilisation Prize \n Updated as of ", Sys.Date()), emph = TRUE)
) 

library(dplyr) # Load the dplyr library for data manipulation
library(shiny) # Load the shiny library for building web applications
library(shinydashboard) # Load the shinydashboard library for creating dashboards in Shiny
library(ggplot2) # Load the ggplot2 library for creating and customizing plots
library(plotly) # Load the plotly library for interactive and dynamic plots
library(rjson) # Load the rjson library for working with JSON data
library(GSODR) 
library(zipcodeR)
library(lubridate)
library(data.table)
library(tidymv)
library(ISLR)
library(mgcv)
library(voxel)
library(tidyverse)
library(gridExtra)
library(xgboost)

setwd("C:\\Users\\matth\\Downloads")  # Set the working directory to the specified path

##---------------------------------------------------------------
##                   Load in Needed Datasets                   --
##---------------------------------------------------------------

# Loading in primary NOAA Data
noaa <- fromJSON(file = "json_temps_zips.json")

# Loading in station load data
stations <- read.csv(file ="substation_data.csv")

# Load in locations that each station provides energy to
locs <- read.csv(file = "locs_doe.csv")

# Loading in building stock data
stock <- read.csv(file = "Stock_DoE.csv")

##---------------------------------------------------------------
##                  Allocate Data to Zip Code                  --
##---------------------------------------------------------------

sub_zips <- as.data.frame(table(locs$SubstationID, locs$Zip))
sub_zips <- sub_zips[which(sub_zips$Freq != 0),]

sub_zips_pre <- sub_zips %>% 
  group_by(Var1) %>% 
  mutate(percent_zip = Freq/sum(Freq)) %>% 
  as.data.frame()

# Load the ISD history dataset from the GSODR package
load(system.file("extdata", "isd_history.rda", package = "GSODR"))

# Extract unique zip codes from data2
zips <- unique(locs$Zip)

# Geocode zip codes to obtain latitude and longitude information
locs_geo <- as.data.frame(geocode_zip(zips))
colnames(locs_geo) <- c("zip", "latitude", "longitude")

# Geocode zip codes to obtain latitude and longitude information
locs <- as.data.frame(geocode_zip(zips))

# Subset the ISD history dataset to include only records from the United States
US <- subset(isd_history, COUNTRY_NAME == "UNITED STATES")

# Filter the records to include only those with an end date after 2021-01-01
US <- US[which(US$END > 20210101)]

# Calculate the distances between latitude and longitude coordinates of locations and US stations
dists_hum <- outer(locs_geo$latitude, US$LAT, function(x, y) sqrt((x - y)^2))
dists_hum <- dists_hum + outer(locs_geo$longitude, US$LON, function(x, y) sqrt((x - y)^2))

# Define a function to find the index of the minimum value in a list
find_min_index_hum <- function(lst) {
  min_index <- which.min(lst)
  return(min_index)
}

# Calculate the minimum distances between locations and US stations, converted to miles
min_dis_hum <- apply(dists_hum, 1, min) * 69
min_dis_hum

# Find the indices of the stations with the minimum distances for each location
min_dis_index_hum <- apply(dists_hum, 1, find_min_index_hum)

# Create a data frame with the zip codes and corresponding stations with minimum distances
min_dis_stations_hum <- cbind("zip" = locs_geo$zip, US[min_dis_index_hum, ])

# Create an empty list to store GSOD weather data for each zip code
min_dis_array_hum <- list()

# Retrieve GSOD weather data for each zip code and year 2020
for (k in 1:nrow(locs_geo)) {
  min_dis_array_hum[[locs_geo$zip[k]]] <- as.data.frame(get_GSOD(years = 2020, station = min_dis_stations_hum$STNID[k]))
}

zips_names <- names(noaa)  # Get the names of the 'noaa' list and assign them to 'zips_names'

for(k in zips_names){
  print(k)  # Print the current element from 'zips_names'
  
  noaa_weather_temp <- noaa[[k]]  # Get the specific element from the 'noaa' list
  
  noaa_weather_temp <- as.data.frame(do.call(rbind, noaa_weather_temp))  # Combine the data frames within the list into a single data frame
  
  noaa_weather_temp <- as.data.frame(lapply(noaa_weather_temp, unlist))  # Unlist all columns in the data frame
  
  noaa_weather_temp$date <- as.Date(noaa_weather_temp$date, format = "%Y%m%d")  # Convert the 'date' column to the date format
  
  noaa_weather_temp$temp_formatted <- as.integer(gsub("[^0-9]", "", noaa_weather_temp$temperature)) / 10  # Extract temperature values and format them
  
  noaa_weather_temp <- noaa_weather_temp %>%  # Select specific columns in the data frame using the pipe operator (%>%)
    select(temp_formatted, wind_speed, visibility_distance, air_pressure, time, date)
  
  noaa_humidity_temp <- min_dis_array_hum[[k]]  # Get the specific element from 'min_dis_array_hum' list
  
  noaa_humidity_temp$YEARMODA <- as.Date(noaa_humidity_temp$YEARMODA)  # Convert the 'YEARMODA' column to the date format
  
  noaa_humidity_temp <- noaa_humidity_temp %>%  # Select specific columns in the data frame using the pipe operator (%>%)
    select(YEARMODA, RH)
  
  noaa_all_temp <- merge(noaa_weather_temp, noaa_humidity_temp, by.x = "date", by.y = "YEARMODA")  # Merge the two data frames based on the 'date' and 'YEARMODA' columns
  
  assign(paste0("noaa_", k), noaa_all_temp)  # Assign the merged data frame to a new variable based on the current element in 'zips_names'
}

# Remove NoAA data due to memory issues
rm(noaa)

##-------------------------------------------------------------------------------------
##  Keep only NOAA data based On closet time the hour of the substation observation  --
##------------------------------------------------------------------------------------

# Iterate over each element in the 'zips_names' vector
for(k in zips_names) {
  print(k)  # Print the current element
  
  # Get data based on a specific pattern (e.g., "noaa_98601")
  data <- get(paste0("noaa_", k))
  
  # Create an empty dataframe to store the subset of data
  subset <- as.data.frame(matrix(NA, nrow = 0, ncol = ncol(data) + 1))
  names(subset) <- c(names(data), "hour")  # Set column names
  
  # Iterate over unique dates in the data
  for(g in unique(data$date)) {
    inner_temp <- data[data$date == g, ]  # Subset data for the current date
    
    inner_temp$time <- as.numeric(inner_temp$time)  # Convert time to numeric
    
    temp_sub <- rep(NA, 24)  # Create a vector to store results for each hour
    
    # Iterate over hours from 0 to 2300 in increments of 100
    for (hour in seq(0, 2300, by = 100)) {
      if (nrow(inner_temp) != 0) {  # Check if there are any rows in the subset
        closest_index <- which.min(abs(inner_temp$time - hour))  # Find the closest index to the current hour
        
        temp_sub[hour/100 + 1] <- closest_index  # Store the closest index
        
        # Add the corresponding row to the subset dataframe
        subset <- rbind(subset, c(inner_temp[closest_index, ], "hour" = hour/100))
      } else {
        temp_sub[hour/100 + 1] <- NA  # If no rows, store NA for the hour
      }
    }
  }
  
  # Assign the resulting subset dataframe to a new variable based on the current element
  assign(paste0("noaa_time_", k), subset)
}

# Create an empty data frame with a column for "zipcode"
noaa_all <- as.data.frame(matrix(NA, nrow = 0, ncol = ncol(noaa_time_98601) + 1))
names(noaa_all) <- c(names(noaa_time_98601), "zipcode")

# Removing missing data
for(k in zips_names){
  # Get the data for the specific zipcode
  data_temp <- get(paste0("noaa_time_", k))
  
  # Remove rows with missing temperature data
  data_temp <- data_temp[which(data_temp$temp_formatted != 999),]
  # Remove rows with missing wind speed data
  data_temp <- data_temp[which(data$wind_speed != 9999),]
  # Remove rows with missing visibility distance data
  data_temp <- data_temp[which(data_temp$visibility_distance != 999999),]
  # Remove rows with missing air pressure data
  data_temp <- data_temp[which(data_temp$air_pressure != 99999),]
  
  # Assign the zipcode value to the data
  data_temp$zipcode <- k
  
  # Append the filtered data to the merged data frame
  noaa_all <- rbind(noaa_all, data_temp)
}

##----------------------------------------------------------------
##                 Building stock data added in                 --
##----------------------------------------------------------------

# Remove the first column from the 'stock' data frame
stock <- stock[,-1]

# Merge the 'noaa_all' and 'stock' data frames using the 'zipcode' column as the common key
data_all <- merge(noaa_all, stock, by.x = "zipcode", by.y = "zip")

# Select columns in 'data_all' starting from the "hour" column and exclude the 9th column
data_all <- data_all[, c("hour", names(data_all)[-9])]

# Create a new column 'datehour' by concatenating the 'date' and 'hour' columns
data_all$datehour <- paste0(data_all$date, "-", data_all$hour)

# Exclude the last column from 'data_all' and include the 'datehour' column
data_all <- data_all[, c("datehour", names(data_all)[-ncol(data_all)])]

# Remove duplicate rows from 'data_all'
data_all <- unique(data_all)

# Create an empty data frame with the same number of columns as 'data_all' plus an additional column 'substation'
data_substation_temp <- as.data.frame(matrix(NA, nrow = 0, ncol = length(names(data_all)) + 1))

# Set column names of 'data_substation_temp' to match the column names of 'data_all' plus 'substation'
names(data_substation_temp) <- c(names(data_all), "substation")

# Iterate over unique values in the 'Var1' column of the 'sub_zips_pre' data frame
for(k in unique(sub_zips_pre$Var1)) {
  # Extract rows from 'sub_zips_pre' where 'Var1' matches the current iteration value 'k'
  stations_temp <- sub_zips_pre[which(sub_zips_pre$Var1 == k),]
  
  # Create an empty data frame 'stations_temp_add' with the same number of columns as 'data_all'
  stations_temp_add <- as.data.frame(matrix(NA, nrow = 0, ncol = length(names(data_all))))
  # Set column names of 'stations_temp_add' to match the column names of 'data_all'
  names(stations_temp_add) <- c(names(data_all))
  
  # Initialize a counter variable 'count'
  count = 0
  
  # Iterate over unique values in the 'Var2' column of 'stations_temp'
  for(zips in unique(stations_temp$Var2)) {
    # Print the current values of 'k', 'zips', and 'count'
    print(paste(k, zips, count))
    # Increment the 'count' by 1
    count = count + 1
    
    # Extract rows from 'stations_temp' where 'Var2' matches the current iteration value 'zips'
    stations_temp_zip <- stations_temp[which(stations_temp$Var2 == zips),]
    # Extract the 'per' column from 'stations_temp_zip'
    per <- stations_temp_zip$per
    
    # Extract rows from 'data_all' where 'zipcode' matches the current iteration value 'zips'
    data_all_temp <- data_all[which(data_all$zipcode == zips),]
    
    # Convert columns 5 to the last column in 'data_all_temp' to numeric type
    data_all_temp[, 5:ncol(data_all_temp)] <- apply(data_all_temp[, 5:ncol(data_all_temp)], MARGIN = 2, as.numeric)
    
    # Multiply columns 5 to the last column in 'data_all_temp' by 'per'
    data_all_temp[, 5:ncol(data_all_temp)] <- data_all_temp[, 5:ncol(data_all_temp)] * per
    
    # If 'count' is not equal to 1, perform the following steps
    if(count != 1) {
      # Get unique values of 'datehour' from 'stations_temp_add' and 'data_all_temp'
      uniquer1 <- unique(stations_temp_add$datehour)
      uniquer2 <- unique(data_all_temp$datehour)
      
      # Filter 'stations_temp_add' to include only rows with 'datehour' values present in 'uniquer2'
      stations_temp_add <- stations_temp_add[which(stations_temp_add$datehour %in% uniquer2),]
      # Filter 'data_all_temp' to include only rows with 'datehour' values present in 'uniquer1'
      data_all_temp <- data_all_temp[which(data_all_temp$datehour %in% uniquer1),]
      
      # Add corresponding columns from 'data_all_temp' to 'stations_temp_add'
      stations_temp_add[, 5:ncol(stations_temp_add)] <- 
        stations_temp_add[, 5:ncol(stations_temp_add)] + data_all_temp[, 5:ncol(data_all_temp)]
    } else {
      # If 'count' is equal to 1, set 'stations_temp_add' to 'data_all_temp'
      stations_temp_add <- data_all_temp
    }
  stations_temp_add$substation <- k
  }
    
  data_substation_temp <- rbind(data_substation_temp, stations_temp_add)
}

stations$hour <- as.numeric(hour(stations$Time))
stations <- stations[,-1]

stations$unique <- paste0(stations$Substation, "-", stations$hour, "-", as.Date(stations$Time))
data_substation_temp$unique <- paste0(data_substation_temp$substation, "-", data_substation_temp$hour, "-",
                                      as.Date(data_substation_temp$date, origin = "1970-01-01"))

write.csv(stations, "temp1.csv")
write.csv(data_substation_temp, "temp2.csv")

stations <- read.csv("temp1.csv")[,-1]
data_substation_temp <- read.csv("temp2.csv")[,-1]

data_all <- merge(stations, data_substation_temp, by = c("unique"))

data_all <- data_all[, ! names(data_all) %in% c("unique", "hour.y", "time", "substation")]

data_all <- rename(data_all, hour = hour.x)

write.csv(data_all, "DOE_data.csv")

##----------------------------------------------------------------
##                     Running the ShinyApp                     --
##----------------------------------------------------------------

setwd("C:\\Users\\matth\\Documents\\Camdus\\DoE\\1653-DOE-Digitalizing-Utilities")

data_all <- read.csv("DOE_data.csv")

##---------------------------------------------------------------
##                      Quantile Analysis                      --
##---------------------------------------------------------------

data_all <- rename(data_all, "Temperature" = "temp_formatted")
data_all$Time <- as.POSIXct(data_all$Time, format = "%Y-%m-%d %H:%M:%S")
data_all$hour <- hour(data_all$Time)
data_all <- data_all[which(data_all$Substation != "STN38"),]

data_all <- na.omit(data_all)
data_all <- data_all[which(data_all$Load > 0),]

data_all$wind_speed[which(data_all$wind_speed == 9999)] <- NA
data_all$visibility_distance[which(data_all$visibility_distance == 999999)] <- NA
data_all$air_pressure[which(data_all$air_pressure == 99999)] <- NA

data_all <- data_all[which(data_all$wind_speed < 100),]
data_all <- data_all[which(data_all$Temperature < 50),]

# Calculate the 95% quantile for each substation
quantiles_df <- data_all %>%
  group_by(Substation) %>%
  summarize(quantile_95 = quantile(Load, probs = 0.95))

# Merge the quantiles back into the original dataframe
data_all_5 <- merge(data_all, quantiles_df, by = "Substation", suffixes = c("", "_sub"))

# Create a new column to indicate if the load is in the 95% quantile for each substation
data_all_5 <- data_all_5 %>%
  mutate(in_95_quantile = Load <= quantile_95)

data_all_5 <- data_all_5[which(data_all_5$in_95_quantile == FALSE),] 
data_all_5 <- data_all_5[,-c(2,26:28)]

sample <- sample(seq(1, nrow(data_all_5)), round(nrow(data_all_5)*0.8))

data_all_5$Time <- as.numeric(data_all_5$Time)
data_all_5$Substation <- as.numeric(as.factor(data_all_5$Substation))

data_all_5 <- data_all_5[,-c(5)]

data_all_5 <- as.matrix(data_all_5)

data_all_5_test <- data_all_5[-sample,]
data_all_5_train <- data_all_5[sample,]

xgb <- xgboost(data = data_all_5_train[,-3], label = (data_all_5_train[,3]), nrounds = 1600, max_depth = 8, eta =0.05)

pred <- predict(xgb, data_all_5_test[,-3])

plot(pred, data_all_5_test[,3]- pred)

mean((data_all_5_test[,3]- pred)^2)

xgb.save(xgb, "Extreme_weather")

xgb.importance(colnames(data_all_5[,-3]),  xgb)

pred_5 <- predict(xgb, data_all_5[,-3])

water_estimate <- read.csv("C:\\Users\\matth\\Documents\\Camdus\\DoE\\water_heater_est.csv")
hvac_estimate <- read.csv("C:\\Users\\matth\\Documents\\Camdus\\DoE\\HAVC_est.csv")

hvac_estimate <- hvac_estimate[, -1]
water_estimate <- water_estimate[, -1]

hvac_estimate <- spread(hvac_estimate, key = pred, value = Freq)

water_estimate$pred <- case_when(
  water_estimate$pred == 0 ~ "No Water Heater",
  water_estimate$pred == 1 ~ "Water Heater"
)

water_estimate <- spread(water_estimate, key = pred, value = Freq)

estimates <- merge(water_estimate, hvac_estimate, by = "Var1")

data_all <- merge(data_all, estimates, by.x = "Substation", by.y = "Var1")

names(data_all) <- gsub(" ", "_", names(data_all))

gam_all <- gam(Load ~ Substation + s(hour) + s(date) + s(Temperature) + s(wind_speed) + s(visibility_distance) +
                 s(RH) + Condos + Mobile.Home + New.Construction +
                 Rural.Residential + mean_sqft + median_yrblt +
                 s(Temperature, hour) + s(RH, hour) + s(RH, Temperature), data = data_all)

summary(gam_all)

saveRDS(gam_all, "saved_model.rds")

fit <- gam_all$fitted.values
value <- data_all$Load

quantiles <- quantile(data_all$Load, probs = c(0.05, 0.95))

df_temp <- data.frame(fit = fit, value = value, res = fit - value)

select_5 <- which(data_all$Load >= quantile(data_all$Load, probs = 0.95, na.rm = TRUE))

df_temp$fit_improved <- df_temp$fit
df_temp$fit_improved[select_5] <- pred_5
df_temp$res_improved <- df_temp$fit_improve - df_temp$value

df_temp <- df_temp[which(data_all$Load != 0 & data_all$Load > 1),]

df_temp <- df_temp[sample_loc,]

gg_basic <- ggplot(df_temp, aes(x = value, y = res)) +
  geom_point(alpha = 0.2, color = "steelblue") + theme_minimal() +
  xlab("Load Value") + ylab("Error") + geom_smooth() +
  ylim(-15, 10) +
  annotate("text", x = 5, y = -12, label = paste0("Mean Absolute Error = ", round(mean(abs(df_temp$res), na.rm = TRUE), digits = 2)))

gg_modeled <- ggplot(df_temp, aes(x = value, y = res_improved)) +
  geom_point(alpha = 0.2, color = "steelblue") + theme_minimal() +
  xlab("Load Value") + ylab("Error") + geom_smooth() +
  ylim(-15, 10) +
  annotate("text", x = 5, y = -12, label = paste0("Mean Absolute Error = ", round(mean(abs(df_temp$res_improved), na.rm = TRUE), digits = 2)))

egg::ggarrange(gg_basic, gg_modeled)

data_view <- data.frame("Humidity" = data_all$RH, "Temperature" = data_all$Temperature, "Effect" = pred$`s(RH,Temperature)`)
data_view <- unique(data_view)

plot_ly(
  x = ~data_view$Humidity, 
  y = ~data_view$Temperature, 
  z = ~data_view$Effect
) %>% 
  layout(
    scene = list(
      xaxis = list(range = c(min(data_view$Humidity), max(data_view$Humidity))),
      yaxis = list(range = c(min(data_view$Temperature), max(data_view$Temperature))),
      zaxis = list(range = c(min(data_view$Effect), max(data_view$Effect)))
    )
  )

plot_ly(
  x = ~data_view$Humidity, 
  y = ~data_view$Temperature, 
  z = ~data_view$Effect,
  colors = colorRamp(c("steelblue", "red"))(100)  # Color scale from steelblue to red
) %>% 
  layout(
    scene = list(
      xaxis = list(title = "Humidity"),  # X-axis label
      yaxis = list(title = "Temperature"),  # Y-axis label
      zaxis = list(title = "Partial Effect"),  # Z-axis label
      aspectmode = "manual",  # Set aspect ratio
      aspectratio = list(x = 1, y = 1, z = 1)  # Equal aspect ratio for all axes
    )
  )

coef_gam <- summary(gam_all)

coef_gam <- data.frame("estimate" = gam_all$coefficients)
coef_gam$count <- seq(1, nrow(coef_gam))

coef_gam_building <- coef_gam[-c(1:41,53:106),]
coef_gam_building$names <- row.names(coef_gam_building)

ggplot(coef_gam_building, aes(x = names, y = estimate)) +
  geom_bar(stat = "identity", fill = "steelblue") + theme_minimal() +
  xlab("Variable Name") + ylab("Coeffecient Estimate") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

vars <- c("hour", "date", "Temperature", "wind_speed", "visibility_distance", "RH")

gam_05 <- gam(Load ~ Substation + s(hour) + s(date) + s(Temperature) + s(wind_speed) + s(visibility_distance) +
                 s(RH) + Commercial + Condos + Industrial + Mobile.Home + Multifamily + New.Construction + Platted.Residential +
                 Rural.Residential + mean_sqft + total_sqft + median_yrblt, data = data_all_5)

saveRDS(gam_05, "saved_model_05.rds")

extreme_weather <- data.frame("Value" = data_all_5$Load, "Estimate" = predict(gam_05, data_all_5))
extreme_weather$Residual <- extreme_weather$Value - extreme_weather$Estimate

ggplot(extreme_weather, aes(x = Value, y = Residual)) +
  geom_point() + theme_minimal()

gam_all_vif_all <- lm(Load ~  Condos + Industrial + Mobile.Home + Multifamily + New.Construction + Platted.Residential +
                        Rural.Residential + mean_sqft + total_sqft + median_yrblt, data = data_all)

building_vif <- as.data.frame(car::vif(gam_all_vif_all))
building_vif$names <- row.names(building_vif)

ggplot(building_vif, aes(x = names, y = car::vif(gam_all_vif_all))) +
  geom_bar(stat = "identity", fill = "steelblue") + xlab("Varaiable Name") + ylab("Variance Inflation Factor") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + theme_minimal() +
  geom_hline(yintercept = 5, color = "red") + coord_flip()

gam_all_vif <- lm(Load ~ Condos + Mobile.Home + New.Construction +
                    Rural.Residential + mean_sqft + median_yrblt, data = data_all)

building_vif <- as.data.frame(car::vif(gam_all_vif))
building_vif$names <- row.names(building_vif)

ggplot(building_vif, aes(x = names, y = car::vif(gam_all_vif))) +
  geom_bar(stat = "identity", fill = "steelblue") + xlab("Varaiable Name") + ylab("Variance Inflation Factor") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + theme_minimal() +
  geom_hline(yintercept = 5, color = "red") + coord_flip()

names_building <- names(data_all)[14:22]
building_data <- data_all[,names_building]
building_data <- unique(building_data)

plot(cor(building_data))

PerformanceAnalytics::chart.Correlation(building_data)

plot(gam_05)

ggplot(data_all, aes(x = Time, y = Load, group = Substation, color = Substation)) +
  geom_line(alpha = 0.2) + theme_minimal()

pred <- gam_all$fitted.values
pred <- as.data.frame(cbind(pred, data_all$Time))

real <- as.data.frame(cbind(data_all$Load, data_all$Time))

names(real) <- names(pred)

data_compare <- rbind(pred, real)
data_compare <- as.data.frame(data_compare)

data_compare$group <- c(rep("Predicted Value", nrow(pred)), rep("Real Value", nrow(real)))

data_compare$V2 <- as.POSIXct(data_compare$V2, origin = "1970-01-01")

ggplot(data_compare, aes(x = V2, y = pred, group = group, color = group)) +
  geom_line(alpha = 0.3) + theme_minimal() + ylab("Load") + xlab("Date") +
  labs(color = "")
