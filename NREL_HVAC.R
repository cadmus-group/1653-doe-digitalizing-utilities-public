# Load necessary libraries
library(readr) # for reading data
library(dplyr) # for data manipulation
library(ggplot2) # for plotting
library(xgboost) # for XGBoost model
library(tidyr) # for reshaping data
library(caret) # for model training and evaluation

# Set working directory to specific path
setwd("C:\\Users\\matth\\Documents\\Camdus\\DoE")

# Read the data file "metadata.tsv" into data_meta
data_meta <- read_tsv("metadata.tsv")

# Select specified columns from data_meta and store into data_sub
data_sub <- data_meta %>% 
  select(c(in.hvac_cooling_type, in.state, in.sqft, in.vintage))

# Remove data_meta from memory to save space
rm(data_meta)
gc() # Garbage collector to reclaim memory

# Convert states to numeric factors
data_sub$in.state <- as.numeric(as.factor(data_sub$in.state))

# Convert 'vintage' values to numeric, with "<1940" being converted to 1940
data_sub$in.vintage <- ifelse(data_sub$in.vintage == "<1940", 1940, data_sub$in.vintage)
data_sub$in.vintage <- gsub("s", "", data_sub$in.vintage) # Remove 's' from vintage
data_sub$in.vintage <- as.numeric(data_sub$in.vintage) # Convert vintage to numeric

# Randomly select 80% of the data for training
select <- sample(nrow(data_sub), round(nrow(data_sub) * 0.8))
data_train <- data_sub[select,] # Training data
data_test <- data_sub[-select,] # Test data (remaining 20%)

# Convert data to matrix
data_train <- as.matrix(data_train)
data_test <- as.matrix(data_test)

# Create factor levels for hvac_type
hvac_factor <- as.data.frame(unique(cbind(data_train[,1], as.numeric(as.factor(data_train[,1])))))
hvac_factor$V2 <- as.numeric(hvac_factor$V2) - 1

# Factorize hvac_type in train and test data
data_train[,1] <- as.numeric(as.factor(data_train[,1])) - 1
data_test[,1] <- as.numeric(as.factor(data_test[,1])) - 1

# Convert all data to numeric
data_train <- apply(data_train, 2, as.numeric)
data_test <- apply(data_test, 2, as.numeric)

# Get class properties
props <- table(data_train[,1])
num_class <- length(unique(data_train[,1]))

# Train XGBoost model
mod_xgb <- xgboost(data = data_train[,2:4], label = data_train[,1], nrounds = 100, 
                   eta = 0.6, num_class = num_class, objective = "multi:softmax")

# Get feature importance
import <- xgb.importance(colnames(data_train[,2:4]), mod_xgb)
import <- gather(import,"Var", "Value", -Feature)

# Plot feature importance
ggplot(import, aes(x = Var, y = Value, fill = Feature)) + 
  geom_bar(stat="identity", position=position_dodge()) +
  theme_minimal()

# Make predictions with the model on test data
pred <- predict(mod_xgb, data_test[,2:4])

# Create a data frame with actual and predicted values
data_out <- data.frame(actual = data_test[,1], predicted = pred)

# Create a confusion matrix to evaluate the model
tab <- table(data_out)
con <- confusionMatrix(tab)

# Save the trained model to a file named "Electrification_Estimates"
xgb.save(mod_xgb, "Electrification_Estimates")
