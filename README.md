
# Shiny Application for DoE Utilisation Prize

This is a detailed Shiny application developed for the Department of Energy Utilisation Prize. It involves sophisticated data manipulation, calculations, and visualizations focused on energy consumption, weather data, and building stock data. Updated as of 2023-07-17.

## Features

- **Data Manipulation and Analysis**: Utilizes R packages such as `dplyr`, `shiny`, `ggplot2`, `plotly`, etc., for effective data handling and analysis.
- **NOAA Weather Data Analysis**: Incorporates NOAA weather data to analyze various climatic factors affecting energy utilization.
- **Building Stock Data Analysis**: Processes and visualizes building stock data to understand energy consumption patterns.
- **Interactive Plots**: Offers dynamic plots for better interpretation and visualization of data.
- **Substation Level Analysis**: Detailed analysis at the substation level for precise energy utilization insights.
- **Future Electrification Estimator**: Predictive models for estimating future electrification needs, focusing on HVAC systems and water heaters.

## Detailed Application Workflow

### User Interface
- **Interactive Dashboard**: The application features a user-friendly dashboard with multiple tabs for different analyses.
- **Customizable Inputs**: Users can select specific parameters and date ranges to customize the data and analyses displayed.

### Data Processing
- **Data Loading**: The app loads data from various sources, including NOAA weather data and building stock data.
- **Data Cleaning and Transformation**: Implements data cleaning, transformation, and aggregation to prepare the data for analysis.

### Modeling and Analysis
- **NOAA Data Processing**: Analyzes weather data to correlate with energy usage patterns.
- **Substation Data Analysis**: Examines substation data to assess energy distribution and consumption.
- **Building Stock Data Integration**: Combines building data with weather and substation data for a comprehensive analysis.
- **Predictive Modeling**: Uses machine learning techniques (e.g., XGBoost, GAM models) to predict future energy needs and electrification potential.
- **Quantile Analysis**: Implements quantile analysis for understanding distribution and extremes in the data.

### Output and Visualization
- **Dynamic Graphs and Charts**: Provides interactive graphs and charts for visualizing data trends and patterns.
- **Substation Level Insights**: Offers detailed insights at the substation level, including energy consumption, predicted needs, and potential areas for electrification.
- **Predictive Outcomes Visualization**: Displays the results of predictive models, highlighting future trends and electrification estimates.

## Installation Instructions

Before running the application, install the necessary R packages as outlined in the previous section.

## How to Run

1. Set the working directory to the application file location.
2. Enable debugging with `reactlog_enable()` if needed.
3. Launch the application using `shinyApp(ui = ui, server = server)`.

## Data Sources and Acknowledgements

The application uses NOAA weather data and building stock data for analysis. It was developed for the Department of Energy Utilisation Prize.

## Author

Cadmus Group
